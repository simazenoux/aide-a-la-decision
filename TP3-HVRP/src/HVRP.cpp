#include <iostream>
#include <vector>

#include "Solution.hpp"
#include "Instance.hpp"

using namespace std;

int main(int argc, char* argv[]) {
	srand(0);

	if (argc != 2) {
		perror("Error : Incorrect number of arguments\n");
		perror("usage : ./main example/paris.txt\n");
		exit(1);
	}

	cout << "Test import instance : ----------------------------------------------------------" << endl;
	Instance instance = Instance(argv[1]);
	cout << instance << endl;

	cout << "Test Tour Géant Aléatoire : --------------------------------------------------------------------------------------------------------------------" << endl;
	TourGeant tourGeantAleantoire = TourGeant(instance.getNombreClients());
	cout << tourGeantAleantoire << endl << endl;

	cout << "Test Tour Géant avec plus proches voisins randomisés mis à 1 : ---------------------------------------------------------------------------------" << endl;
	TourGeant tourGeantPlusProcheVoisin1 = TourGeant(instance, 1);
	cout << tourGeantPlusProcheVoisin1 << endl << endl;

	cout << "Test Tour Géant avec plus proches voisins randomisés mis à 5 : ---------------------------------------------------------------------------------" << endl;
	TourGeant tourGeantPlusProcheVoisin5 = TourGeant(instance, 5);
	cout << tourGeantPlusProcheVoisin5 << endl << endl;

	cout << "Test Solution : --------------------------------------------------------------------------------------------------------------------------------" << endl;

	
	Solution solutionTourGeantAleatoire = Solution(instance, tourGeantAleantoire);
	cout << solutionTourGeantAleatoire << endl;

	Solution solutionTourGeantPlusProcheVoisin1 = Solution(instance, tourGeantPlusProcheVoisin1);
	cout << solutionTourGeantPlusProcheVoisin1 << endl;

	// Solveur solveur = Solveur(instance);
	// solveur.afficher();

	// solveur.GRASP(1000, 100);


	// cout << "Test génération tour géant aléatoire : ------------------------------" << endl;
	// TourGeant tourGeant = TourGeant(10);
	// instance.afficher();
	

	return 0;
}
