#include "Solveur.hpp"


int sommeCouts(vector<Solution> solutions)
{
	int somme = 0;
	for (auto &solution : solutions)
	{
		somme += solution._coutTotal;
	}
	return somme;
}


Solveur::Solveur(Instance instance): _instance(instance)
{}



void Solveur::rechercheLocale(int maximumNumberOfIterations, int maximumNumberOfIterationsWithoutChange)
{
	vector<float> probabilities = vector<float>(4, 0.5f);
    probabilities[2] = -1;
	vector<int> changes = vector<int>(4);

    std::uniform_real_distribution<double> unif(0,1);
    std::default_random_engine re;

	int somme;
	int meilleurSommeCout = sommeCouts(solutions);
	int noChange = 0;
    int numberOfIterations = 0;
    int numberOfIterationsWithoutChange = 0;
	
	while (numberOfIterations++ < maximumNumberOfIterations && numberOfIterationsWithoutChange++ < maximumNumberOfIterationsWithoutChange)
	{

		fill(changes.begin(), changes.end(), 0);

        double p = unif(re);

		if (p < probabilities[0]) {
			for (int j = 0; j < solutions.size(); j++)
				changes[0] += innerSwap(_instance,solutions[j]);
			if (changes[0] == 0) probabilities[0] = max(probabilities[0] - PROBABILITY_DELTA, PROBABILITY_LOWER_BOUND);
			if (changes[0] > PROBABILITY_CHANGES_TRIGGER) probabilities[0] = min(probabilities[0] + PROBABILITY_DELTA, PROBABILITY_UPPER_BOUND);
		}
		else if (p < probabilities[0] + probabilities[1]) {
			for (int j = 0; j < solutions.size(); j++)
				changes[1] += innerInsert(_instance, solutions[j]);
			if (changes[1] == 0) probabilities[1] = max(probabilities[1] - PROBABILITY_DELTA, PROBABILITY_LOWER_BOUND);
			if (changes[1] > PROBABILITY_CHANGES_TRIGGER) probabilities[1] = min(probabilities[1] + PROBABILITY_DELTA, PROBABILITY_UPPER_BOUND);
		}
		else if (p < probabilities[0] + probabilities[1] + probabilities[2]) {
			for (int j = 0; j < solutions.size(); j++)
				for (int k = j + 1; k < solutions.size(); k++)
					changes[2] += interSwap(_instance, solutions[j], solutions[k]);
			if (changes[2] == 0) probabilities[2] = max(probabilities[2] - PROBABILITY_DELTA, PROBABILITY_LOWER_BOUND);
			if (changes[2] > PROBABILITY_CHANGES_TRIGGER) probabilities[2] = min(probabilities[2] + PROBABILITY_DELTA, PROBABILITY_UPPER_BOUND);
		}
		else {
			for (int j = 0; j < solutions.size(); j++)
				for (int k = j + 1; k < solutions.size(); k++)
					changes[3] += interInsert(_instance, solutions[j], solutions[k]);
			if (changes[3] == 0) probabilities[3] = max(probabilities[3] - PROBABILITY_DELTA, PROBABILITY_LOWER_BOUND);
			if (changes[3] > PROBABILITY_CHANGES_TRIGGER) probabilities[3] = min(probabilities[3] + PROBABILITY_DELTA, PROBABILITY_UPPER_BOUND);
		}
	}

	somme = sommeCouts(solutions);
	if (somme < meilleurSommeCout) 
	{
		meilleurSommeCout = somme;
		numberOfIterationsWithoutChange = 0;
	}

	// TG = _instance::InvSPLIT(solution, _instance);
}


void Solveur::GRASP(int maxIteration, int noChangeMaxIteration)
{
	// int bestCout, currCout, coutBestVoisin, tmp, i1, i2, iterationVoisins;

	// // vector<Solution> solutions;
	// Solution currSol, solBest;
	// TourGeant tmpTG, bestTG;

	// TourGeant tourGeant = TourGeant(*this);
	// currSol = Solution(*this, tourGeant);

	// bestCout = rechercheLocale(100, 100);
	// int currHash = solution.hash();
	// hash[currHash] = 1;


	// for (int i = 0; i < maxIteration; i++)
	// {
	// 	cout << (float)i * 100 / maxIteration << "%" << "\t->\t" << bestCout << " " << TG.size() << endl;
 	// 	coutBestVoisin = INT32_MAX;
	// 	for (int j = 0; j < 10; j++) // nb voisins
	// 	{
	// 		iterationVoisins = 0;
	// 		do
	// 		{
	// 			iterationVoisins++;
	// 			currSol = Solution(solution);
	// 			tmpTG = TourGeant(TG);
	// 			i1 = 1 + (rand() % (tmpTG.size() - 2));
	// 			do
	// 			{
	// 				i2 = 1 + (rand() % (tmpTG.size() - 2));
	// 			} while (i1 == i2);
				
	// 			tmp = tmpTG[i1];
	// 			tmpTG[i1] = tmpTG[i2];
	// 			tmpTG[i2] = tmp;

	// 			currCout = rechercheLocale(100, 100);
	// 			tmp = solution.hash();
	// 		} while (!hash[tmp] == 1 && iterationVoisins < 100);
	// 		hash[tmp] = 1;

	// 		if (currCout < coutBestVoisin)
	// 		{
	// 			coutBestVoisin = currCout;
	// 			solBest = Solution(currSol);
	// 			bestTG = TourGeant(tmpTG);
	// 		}
	// 	}

	// 	if (coutBestVoisin < bestCout)
	// 	{
	// 		TG = TourGeant(bestTG);
	// 		bestCout = coutBestVoisin;
	// 		solution = Solution(solBest);
	// 	}
	// }
}


void insert(vector<Solution>& solutions, const Solution &solution)
{
	const int MAX_SIZE = 5;

	lower_bound(solutions.begin(), solutions.end(), solution), solution;

	// Si on dépasse la taille max
	if (solutions.size() >= MAX_SIZE)
	{
		// On supprime la pire solution
		solutions.pop_back();
	}
}


void move(vector<int>& tour, int from, int to)
{
	int tmp = tour[from];
	if (from > to) {
		for (int i = from; i > to; i--)
			tour[i] = tour[i - 1];
		tour[to] = tmp;
	}
	else {
		for (int i = from; i < to; i++)
			tour[i] = tour[i + 1];
		tour[to] = tmp;
	}
}

void inverserIntervalle(int* a, int* b)
{
	if (a > b)
	{
		swap(a,b);
	}

	while (a < b) {
		swap(a,b);
		a++;
		b--;
	}
}

bool Solveur::innerSwap(Instance _instance, Solution& solution)
{
	bool improvement = false;
	int delta1, delta2;
	int beta1, beta2;
	int deltaTotal;

	for (int i = 1; i < _instance.nombreClients; i++) {
		delta1 = _instance.distance[solution._tourGeant._t[i]][solution._tourGeant._t[i + 1]];
		for (int j = i + 1; j <= _instance.nombreClients; j++) {
			delta2 = _instance.distance[solution._tourGeant._t[j]][solution._tourGeant._t[j + 1]];
			beta1 = _instance.distance[solution._tourGeant._t[i]][solution._tourGeant._t[j]];
			beta2 = _instance.distance[solution._tourGeant._t[i + 1]][solution._tourGeant._t[j + 1]];
			deltaTotal = beta1 + beta2 - delta1 - delta2;
			if (deltaTotal < 0) {
				inverserIntervalle(&(solution._tourGeant._t[i]), &(solution._tourGeant._t[j]));
				solution._coutTotal -= deltaTotal;
				improvement = true;
			}
		}
	}
	return improvement;
}


bool Solveur::innerInsert(Instance _instance, Solution& solution)
{
	bool improvement = false;
	int delta1, delta2;
	int deltaTotal;

	for (int i = 1; i < _instance.nombreClients; i++)
	{
		delta1 = _instance.distance[solution._tourGeant._t[i - 1]][solution._tourGeant._t[i + 1]] - _instance.distance[solution._tourGeant._t[i - 1]][solution._tourGeant._t[i]] - _instance.distance[solution._tourGeant._t[i]][solution._tourGeant._t[i + 1]];
		for (int j = i + 1; j <= _instance.nombreClients; j++)
		{
			delta2 = _instance.distance[solution._tourGeant._t[j - 1]][solution._tourGeant._t[j]] + _instance.distance[solution._tourGeant._t[j]][solution._tourGeant._t[j + 1]] - _instance.distance[solution._tourGeant._t[j - 1]][solution._tourGeant._t[j + 1]];
			deltaTotal = delta1 + delta2;
			if (deltaTotal < 0) {
				move(solution._tourGeant._t, j, i);
				solution._coutTotal -= deltaTotal;
				improvement = true;
			}
		}
	}
	return improvement;
}


bool Solveur::interSwap(Instance _instance, Solution& sol1, Solution& sol2)
{
	// vector<int> tmp1 = vector<int>(_instance.nombreClients);
	// vector<int> tmp2 = vector<int>(_instance.nombreClients);

	// bool improvement = false;


	// vector<int> tour1 = sol1._tourGeant.getTourGeant();
	// vector<int> tour2 = sol2._tourGeant.getTourGeant();

	// int tour1Len = sol1._tourGeant.getTaille();
	// int tour2Len = sol2._tourGeant.getTaille();
	// int minus1, minus2, minus3, minus4;
	// int plus1, plus2, plus3, plus4;
	// int deltaTotal;
	// int weight1, weight2, totalWeight1, totalWeight2;
	// int changes = 0;
	// TourGeant tourGeant = TourGeant(sol1._tourGeant.getTourGeant());
	// Solution solution = Solution(_instance, tourGeant);

	// for (int i = 1; i < _instance.nombreClients; i++)
	// {
	// 	minus1 = _instance.distance[tour1[i]][tour1[i + 1]];
	// 	for (int j = i + 1; j < _instance.nombreClients; j++)
	// 	{
	// 		weight1 = 0;
	// 		minus2 = _instance.distance[tour1[j]][tour1[j + 1]];
	// 		for (int e = i + 1; e <= j; e++) {
	// 			weight1 += _instance.demande[tour1[e]];
	// 			tmp1[e - i - 1] = tour1[e];
	// 		}


	// 		for (int k = 1; k < _instance.nombreClients - 2; k++)
	// 		{
	// 			minus3 = _instance.distance[tour2[k]][tour2[k + 1]];
	// 			for (int l = k + 1; l < tour2Len - 2; l++)
	// 			{
	// 				weight2 = 0;
	// 				minus4 = _instance.distance[tour2[l]][tour2[l + 1]];
	// 				for (int e = k + 1; e <= l; e++) {
	// 					tmp2[e - k - 1] = tour2[e];
	// 					weight2 += _instance.demande[tour2[e]];
	// 				}

	// 				plus1 = _instance.distance[tour1[i]][tour2[l]];
	// 				plus2 = _instance.distance[tour1[j]][tour2[k]];
	// 				plus3 = _instance.distance[tour1[i + 1]][tour2[l + 1]];
	// 				plus4 = _instance.distance[tour1[j + 1]][tour2[k + 1]];
	// 				deltaTotal = plus1 + plus2 + plus3 + plus4 - minus1 - minus2 - minus3 - minus4;

	// 				if (deltaTotal < 0) {
	// 					totalWeight1 = max(solution.poidTours[indexTour1] + weight2 - weight1, 0);
	// 					totalWeight2 = max(solution.poidTours[indexTour2] + weight1 - weight2, 0);
	// 					if (totalWeight1 < _instance.capacite && totalWeight2 < _instance.capacite) {
	// 						for (int counter = 0, e = i + 1; e <= j; e++, counter++) tour1[e] = tmp2[counter];
	// 						for (int counter = 0, e = k + 1; e <= l; e++, counter++) tour2[e] = tmp1[counter];
	// 						Tour::clean(tour1);
	// 						Tour::clean(tour2);

	// 						inverserIntervalle(&tour1[i + 1], &tour1[j]);
	// 						inverserIntervalle(&tour2[k + 1], &tour2[l]);

	// 						tour1Len = solution.toursLength[indexTour1] = tour1.size();
	// 						tour2Len = solution.toursLength[indexTour2] = tour2.size();

	// 						sol1._cout = max(solution.coutTours[indexTour1] + plus1 + plus4 - minus1 - minus2, 0);
	// 						sol2._cout = max(solution.coutTours[indexTour2] + plus2 + plus3 - minus3 - minus4, 0);

	// 						solution.poidTours[indexTour1] = totalWeight1;
	// 						solution.poidTours[indexTour2] = totalWeight2;

	// 						improvement = true;
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// }
	// return improvement;

	return false;
}



bool Solveur::interInsert(Instance _instance, Solution& solution1, Solution& solution2)
{
	// int* tour1 = solution1._tourGeant._t;
	// int* tour2 = solution2._tourGeant._t;

	// int tour1Len = solution1._tourGeant._taille;
	// int tour2Len = solution2._tourGeant._taille;
	// int minus1, minus2, minus3;
	// int plus1, plus2, plus3;
	// int deltaTotal;


	// for (int i = 1; i < tour1Len - 1; i++)
	// {
	// 	if (tour1Len < 3)
	// 		continue;

	// 	minus1 = _instance.distance[tour1[i - 1]][tour1[i]];
	// 	minus2 = _instance.distance[tour1[i]][tour1[i + 1]];
	// 	plus1 = _instance.distance[tour1[i - 1]][tour1[i + 1]];
	// 	for (int j = 0; j < tour2Len - 2; j++)
	// 	{
	// 		if (tour2Len < 3 || _instance.demande[tour1[i]] + solution.poidTours[indexTour2] > _instance.truckCapacity) {
	// 			continue;
	// 		}
	// 		minus3 = _instance.distance[tour2[j]][tour2[j + 1]];
	// 		plus2 = _instance.distance[tour2[j]][tour1[i]];
	// 		plus3 = _instance.distance[tour1[i]][tour2[j + 1]];
	// 		deltaTotal = plus1 + plus2 + plus3 - minus1 - minus2 - minus3;
	// 		if (deltaTotal < 0) {
	// 			solution.coutTours[indexTour2] += deltaTotal;
	// 			solution.poidTours[indexTour1] -= _instance.demande[tour1[i]];
	// 			solution.poidTours[indexTour2] += _instance.demande[tour1[i]];
	// 			tour2.push_back(tour1[i]);
	// 			utils::move(tour2, tour2Len, j + 2);
	// 			tour1.erase(tour1.begin() + i);

	// 			solution.toursLength[indexTour1]--;
	// 			tour1Len = solution.toursLength[indexTour1];
	// 			solution.toursLength[indexTour2]++;
	// 			tour2Len = solution.toursLength[indexTour2];

	// 			changes++;
	// 		}
	// 	}
	// }

	return false;
}



ostream& operator<<(ostream& os, const Solveur& s)
{
    os << s._instance << endl;
    os << "Solutions : " << endl;
    for(auto& solution: s.solutions)
	{
        os << solution << endl;
    }
	return os;
}