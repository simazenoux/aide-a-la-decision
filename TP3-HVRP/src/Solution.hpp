#ifndef SOLUTION_HPP
#define SOLUTION_HPP

#include <string>
#include <iostream>
#include <fstream>

#include "TourGeant.hpp"
#include "Instance.hpp"


#define HASH_SIZE 10000

using namespace std;

class Solution
{
    public:
        TourGeant _tourGeant;
        vector<int> _pere;
        vector<int> _cout;
        int _coutTotal;
        int64_t _hash;

    private:
        int split(Instance i);
        int64_t hash();

    public:
        Solution(Instance instance, TourGeant tourGeant);

        friend ostream& operator<<(ostream&, const Solution&);
        friend bool operator<(Solution const &a, Solution const &b);
};

#endif

