#ifndef INSTANCE_HPP
#define INSTANCE_HPP


#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <vector>

#define N 300
#define HASH_SIZE 10000


#define MAX_ITERATION_INNER_2OP 200
#define PROBABILITY_LOWER_BOUND 0.2
#define PROBABILITY_UPPER_BOUND 0.8
#define PROBABILITY_DELTA 0.05
#define PROBABILITY_CHANGES_TRIGGER 5

using namespace std;

class Instance {
    public:
        int nombreClients;
        int distance[N][N];
        int demande[N];
        int capacite;

    public:
        Instance(string filename);

        int getNombreClients();
        void rechercheLocale(int maxIteration = 20, int noChangeMaxIteration = 4);
		void GRASP(int maxIteration = 1000, int noChangeMaxIteration = 10000);
	    
        friend ostream& operator<<(ostream&, const Instance&);
};

#endif