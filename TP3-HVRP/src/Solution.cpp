#include "Solution.hpp"


Solution::Solution(Instance instance, TourGeant tourGeant): _tourGeant(tourGeant), _coutTotal(0), _hash(0)
{
	_coutTotal = split(instance);
	hash();
}


int64_t Solution::hash()
{
    _hash = 0;
    for (const int& cout: _cout)
    {
        _hash += pow(cout,2);
    }

    return _hash;
}


int Solution::split(Instance instance) 
{
    _cout = vector<int>(_tourGeant.getTaille());
    _pere = vector<int>(_tourGeant.getTaille());
    
    _cout[0] = 0;
    for(int i=1; i<instance.nombreClients; i++)
    {
        _cout[i] = INT32_MAX;
    }

    _pere[0] = 0;

    int j;
    int cout, capacite;
    for (int i=0; i < instance.nombreClients+1; i++)
    {
        j = i+i;
        do
        {
            if (j==i+1)
            {
                
                cout = instance.distance[0][_tourGeant._t[j]] + instance.distance[_tourGeant._t[j]][0];
                capacite = instance.demande[_tourGeant._t[j]];
            }
            else
            {
                cout += instance.distance[_tourGeant._t[j-1]][_tourGeant._t[j]] + instance.distance[_tourGeant._t[j]][0] - instance.distance[_tourGeant._t[j-1]][0];
                capacite += instance.demande[_tourGeant._t[j]];
            }
            if (_cout[_tourGeant._t[i]] + cout < _cout[_tourGeant._t[j]])
            {
                _cout[_tourGeant._t[j]] = _cout[_tourGeant._t[i]] + cout;
                _pere[_tourGeant._t[j]] = _tourGeant._t[i];
            }
            j++;
        } while (j < instance.nombreClients +2 && capacite + instance.demande[_tourGeant._t[j]] <= instance.capacite);
    }

	for (int i = 0; i < instance.nombreClients+1; i++)
	{
		_coutTotal += _cout[i];
	}

	return _coutTotal;
}





bool operator<(Solution const &a, Solution const& b)
{
    return a._cout < b._cout;
}



ostream& operator<<(ostream& os, const Solution& s)
{
    os << "Tour géant : " << s._tourGeant << endl;
	os << "Cout total : " << s._coutTotal << endl;
	os << "Hash       : " << s._hash << endl;
    return os;
}
