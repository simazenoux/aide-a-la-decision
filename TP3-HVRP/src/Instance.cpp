#include "Instance.hpp"


Instance::Instance(string filename)
{
    ifstream file(filename, ios::in);   

    if (!file)
    {
        throw invalid_argument("Could not open file");
    }

    file >> nombreClients;
    file >> capacite;

    for (int i=0; i <= nombreClients; i++)
    {
        for (int j=0; j <= nombreClients; j++)
        {
            file >> distance[i][j];
        }
    }

    demande[0] = 0;
    for (int i=1; i<=nombreClients; i++)
    {
        int garbage;
        file >> garbage >> demande[i];
    }
}

int Instance::getNombreClients()
{
	return nombreClients;
}



ostream& operator<<(ostream& os, const Instance& instance)
{
	os << "Capacité des camions\t" << instance.capacite << endl;
	for (int i = 0; i <= instance.nombreClients; i++)
	{
		os << "Ville " << i << "\t, Demande = " << instance.demande[i] << endl;
		for (int j = 0; j <= instance.nombreClients; j++) 
		{
			os << instance.distance[i][j] << "\t";
		}
		os << endl;
	}

    return os;
}

