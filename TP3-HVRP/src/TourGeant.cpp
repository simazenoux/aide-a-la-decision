#include "TourGeant.hpp"

// Tour géant aléatoire
TourGeant::TourGeant(int nombreClients)
{
    vector<int> np(nombreClients);
    iota(np.begin(),np.end(), 1);

	_t.push_back(0);
    while (!np.empty())
    {
		swap(np.at(rand() % np.size()), np.back());
        _t.push_back(np.back());
		np.pop_back();
    }
	_t.push_back(0);
}


// // Tour géant plus proches voisins
TourGeant::TourGeant(Instance instance, int nombreVoisins=5)
{

	vector<int> tab(instance.nombreClients);
	vector<int> neighborsdistance(nombreVoisins + 1);
	vector<int> neighborsID(nombreVoisins + 1);

	int length = instance.nombreClients;
	int random = 0;
	int counter = 0;
	int neighborCounter;
	int nombreVoisinsTrouves;

	for (int i = 0; i <= instance.nombreClients; i++)
		tab[i] = i;

	_t.push_back(0);
	for (; length > 0; length--) {

		for (int i = 0; i < nombreVoisins + 1; i++) 
		{
			neighborsdistance[i] = INT32_MAX;
			neighborsID[i] = -1;
		}

		for (int i = 1; i <= instance.nombreClients; i++) 
		{
			if (tab[i] == -1 || instance.distance[random][tab[i]] == 0)
				continue;
			neighborCounter = nombreVoisins;
			neighborsdistance[neighborCounter] = instance.distance[random][tab[i]];
			neighborsID[neighborCounter] = tab[i];
			
			while (neighborCounter > 0 && neighborsdistance[neighborCounter] < neighborsdistance[neighborCounter - 1]) {
				swap(neighborsID[neighborCounter], neighborsID[neighborCounter - 1]);
				swap(neighborsdistance[neighborCounter], neighborsdistance[neighborCounter - 1]);
				neighborCounter--;
			}
		}

		nombreVoisinsTrouves = 0;
		for (int i = 0; i < nombreVoisins; i++)
			if (neighborsID[i] != -1)
				nombreVoisinsTrouves++;
		if (nombreVoisinsTrouves == 0) 
			break;
		random =  rand() % nombreVoisinsTrouves;
		random = neighborsID[random];

		_t.push_back(tab[random]);
		tab[random] = -1;
	}
	_t.push_back(0);
}



// Tour géant plus proches voisins
// TourGeant::TourGeant(Instance instance, int nombreVoisins=5)
// {
// 	vector<int> np(instance.nombreClients);
//     iota(np.begin(),np.end(), 1);

// 	_t.push_back(0);
// 	while (!np.empty())
//     {
// 		vector< pair<int, int> > missingElements;
// 		for (int &i : np)
// 		{
// 			missingElements.push_back(make_pair(instance.distance[_t.back()][i], i));
// 		}
// 		sort(missingElements.begin(), missingElements.end());

// 		// int i = min( (int) missingElements.size()-1, nombreVoisins);
// 		int index = missingElements[0].second;
		
// 		swap(np.at(index), np.back());
//         _t.push_back(np.back());
// 		np.pop_back();
//     }
// 	_t.push_back(0);
// }


TourGeant::TourGeant(vector<int> t): _t(t)
{}


vector<int> TourGeant::getTourGeant()
{
    return _t;
}

int TourGeant::getTaille()
{
    return _t.size();
}


ostream& operator<<(ostream& os, const TourGeant& tg)
{
	for (int i = 0; i < tg._t.size(); i++)
	{
		os << tg._t[i] << ' ';
	}
	
	return os;
}

