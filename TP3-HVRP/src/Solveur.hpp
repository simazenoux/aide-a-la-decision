#ifndef SOLVEUR_HPP
#define SOLVEUR_HPP

#include <iostream>
#include <set>
#include <vector>

#include "Instance.hpp"
#include "TourGeant.hpp"
#include "Solution.hpp"

using namespace std;

class Solveur
{
    private:
        Instance _instance;
        vector<Solution> solutions;
        set<int> hash;

    public:
        Solveur(Instance instance);

        static bool innerSwap(Instance instance, Solution& solution);
		static bool innerInsert(Instance instance, Solution& solution);
		static bool interSwap(Instance instance, Solution& solution1, Solution& solution2);
		static bool interInsert(Instance instance, Solution& solution1, Solution& solution2);

        void rechercheLocale(int maximumNumberOfIterations, int maximumNumberOfIterationsWithoutChange);
        void GRASP(int maxIteration, int noChangeMaxIteration);
        
        friend ostream& operator<<(ostream& os, const Solveur& s);
};


#endif