#ifndef TOUR_GEANT_HPP
#define TOUR_GEANT_HPP

#include <algorithm>
#include <iterator>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

#include "Instance.hpp"

using namespace std;

class TourGeant {
    public:
        vector<int> _t;

    public:
        TourGeant(int nombreClients);
        TourGeant(Instance i, int nombreVoisins);
        TourGeant(vector<int> t);

        vector<int> getTourGeant();
        int getTaille();

        friend ostream& operator<<(ostream&, const TourGeant&);
};

#endif