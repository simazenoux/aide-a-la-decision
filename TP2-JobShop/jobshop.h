#ifndef JOBSHOP_H
#define JOBSHOP_H

#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <tuple>

#define NMAX 50
#define MMAX 50
#define NMMAX NMAX*MMAX

const int nmax = 50;
const int mmax = 50;
const int infini = INT32_MAX;


using namespace std;


typedef struct Solution
{
    int id;
	int V[NMAX * MMAX]; 
	int St[NMAX][MMAX] = { 0 };
	tuple<int, int> pere[NMAX][MMAX] = { make_tuple(0,0) };
	int cout = 0;
} Solution_T;


typedef struct Instance
{
	int n;
	int m;
	int M[nmax + 1][mmax + 1] = { 0 };
	int P[nmax + 1][mmax + 1] = { 0 }; 

} Instance_T;


// void lecture(string, Instance_T&);
// void affichage_instance(Instance_T);
// void generer_bierwith_alea(Instance_T&, Vecteur_T&);
// void affichage_vecteur(Vecteur_T);
// void evaluer_bierwith(Instance_T&, Vecteur_T&);
// void recherche_locale(Instance_T&,Vecteur_T&,int);

#endif