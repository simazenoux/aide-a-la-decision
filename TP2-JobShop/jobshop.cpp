#include "jobshop.h"


bool importJobShop(string pathFile, Instance_T * instance)
{
    ifstream file(pathFile, ios::in);   

    if (!file)
    {
        cout << "Import failed" << endl;
        return false;
    }

    file >> instance->n;
    file >> instance->m;

    for (int i=0; i< instance->n; i++)
    {
        for (int j=0; j< instance->m; j++)
        {
            file >> instance->M[i][j];
            file >> instance->P[i][j];
        }
    }

    cout << "Import successful" << endl;
    return true;
}


void generateBierwithVector(int n, int m, int lambda[])
{
    int np[NMAX];
    int nc[NMAX];

    for (int i=0; i<n; i++)
    {
        np[i]=i;
        nc[i] = m;
    }

    int size = n;
    for (int i=0; i<n*m; i++)
    {
        int index = rand() % size;
        lambda[i] = np[index];
        if (--nc[index]==0)
        {
            size--;
            np[index] = np[size];
            nc[index] = nc[size];
        }
    }
}


void test_displayJobShop(Instance_T instance)
{
    cout << instance.n << " " << instance.m << endl;
    for (int i=0; i< instance.n; i++)
    {
        for (int j=0; j< instance.m; j++)
        {
            cout << instance.M[i][j] << " ";
            cout << instance.P[i][j] << " ";
        }
        cout << endl;
    }
}

void test_importJobShop(string pathFile)
{

    Instance_T i;
    if (importJobShop(pathFile, &i))
    {
        test_displayJobShop(i);
    }
}


void test_generateBierwithVector(int n, int m)
{
    // int lambda[NMAX*MMAX];
    int lambda[NMAX];

    for (int i=0; i<5; i++)
    {
        generateBierwithVector(n, m, lambda);
        for (int j=0; j<n*m; j++)
        {
            cout << lambda[j] << " ";
        }
        cout << endl;
    }
}


Solution_T evaluer(Instance_T instance, int * bierwithVector)
{
    Solution_T solution;

    
}

// int evaluer(Instance_T instance, int * bierwithVector)
// {
// 	// On nettoie les tableaux au cas où il existe deja avant
// 	for (unsigned i=0;i < tabItem_.size();i++) {
// 		tabItem_[i].machines.clear();
// 		tabItem_[i].last_op = nullptr;
// 		tabItem_[i].duree = 0;
// 	}		// Nettoyage du vecteur des pièces

// 	for (unsigned i=0;i < tabOpe_.size();i++) {
// 		tabOpe_[i].jobs.clear();
// 		tabOpe_[i].last_op = nullptr;
// 		tabOpe_[i].duree = 0;
// 	}		// Nettoyage du vecteur des machines


// 	// Pour le premier job
// 	cur = bierwirth_vector_[0];
// 	cur->prev_ = nullptr;									// Pas de précédent
// 	cur->father_ = nullptr;									// Pas de père car c'est le premier
// 	cur->starting_ = 0;										// Démarre à t = 0
// 	cur->location_ = 0;										// Premier élément du tableau

// 	// Mise à jour des données par machine
// 	tabOpe_[cur->machine_].jobs.push_back(cur);				// On l'ajoute à la machine
// 	tabOpe_[cur->machine_].last_op = cur;					// C'est la dernière opération
// 	tabOpe_[cur->machine_].duree += cur->duration_;			// Mise à jour de la durée

// 	// Mise à jour des données par pièce
// 	tabItem_[cur->item_].machines.push_back(cur);			// Première opération pour la pièce
// 	tabItem_[cur->item_].last_op = cur;						// C'est la dernière opération
// 	tabItem_[cur->item_].duree += cur->duration_;			// Mise à jour de la durée
	
// 	for (unsigned i = 1; i < bierwirth_vector_.size(); i++) {
// 		// Mise en place des variables
// 		cur = bierwirth_vector_[i];
// 		machine_tmp = &(tabOpe_[cur->machine_]);
// 		item_tmp = &(tabItem_[cur->item_]);

// 		// Chainage
// 		//bierwirth_vector_[i - 1]->next_ = cur;					// Chainage suivant
// 		//cur->prev_ = bierwirth_vector_[i - 1];					// Chainage précédent
// 		//ici prev est redondant pusiqu'on a deja i
		
// 		if (machine_tmp->last_op != nullptr) machine_tmp->last_op->next_ = cur;
// 		cur->prev_ = machine_tmp->last_op;						// Chainage précédent : 

// 		cur->location_ = i;										// Enregistrement de la place dans Bierwirth

// 		// Gestion du temps
// 		if (machine_tmp->duree > item_tmp->duree) {				// On attend la machine
// 			time = machine_tmp->duree;
// 			wait_machine = 0;									// La machine n'attend pas
// 			wait_item = machine_tmp->duree - item_tmp->duree;	// La pièce attend après la machine

// 			// Father est le dernier job exécuté par la machine
// 			cur->father_ = (machine_tmp->jobs.size() < 1) ? nullptr
// 				:machine_tmp->jobs.at(machine_tmp->jobs.size() - 1);
// 						// Si on n'a pas encore d'éléments on met nullptr
// 		}else if (machine_tmp->duree == item_tmp->duree) {		// Pas d'attente
// 			time = machine_tmp->duree;
// 			wait_machine = 0;
// 			wait_item = 0;

// 			// Choix arbitraire ? Je prend la pièce comme référence
			
// 			cur->father_ = (std::max(item_tmp->machines.size(), machine_tmp->jobs.size()) < 1) ? nullptr
// 				: (item_tmp->machines.size() >= machine_tmp->jobs.size()) ?
// 				item_tmp->last_op
// 				: machine_tmp->last_op;
// 		}else {														// On attend la pièce d'une autre machine
// 			time = item_tmp->duree;
// 			wait_machine = item_tmp->duree - machine_tmp->duree;	// La machine est obligée d'attendre
// 			wait_item = 0;											// La pièce n'attend pas
// 			// Father est la dernière opération effectuée sur la pièce
// 			cur->father_ = (item_tmp->machines.size() < 1) ? nullptr
// 				: item_tmp->machines.at(item_tmp->machines.size() - 1);
// 			// Si on n'a pas encore d'éléments on met nullptr
// 		}

// 		// Mise à jour des données par machine
// 		machine_tmp->jobs.push_back(cur);
// 		machine_tmp->last_op = cur;
// 		machine_tmp->duree += wait_machine + cur->duration_;

// 		// Mise à jour des données par pièce
// 		item_tmp->machines.push_back(cur);
// 		item_tmp->last_op = cur;
// 		item_tmp->duree += wait_item + cur->duration_;

// 	}	// Fin for principal

// 	// Evaluation du makespan et de last_op
// 	makespan_ = tabItem_[0].duree;
// 	last_cp_ = tabItem_[0].last_op;								// Par défaut on prend la première pièce

// 	for (unsigned i = 0; i < tabItem_.size(); i++)
// 	{
// 		if (tabItem_[i].duree > makespan_) {
// 			makespan_ = tabItem_[i].duree;						// Mise à jour du makespan
// 			last_cp_ = tabItem_[i].last_op;						// Mise à jour de la dernière opération
// 		}
// 	}
// }

int main()
{
    // srand(time(NULL));
    srand(123);

    cout << NMMAX;
    test_generateBierwithVector(3,4);
    test_importJobShop("examples/la01.txt");

    return 0;
}